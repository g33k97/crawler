<?php

use App\Http\Controllers\AuthController;
use App\Http\Controllers\CategoryController;
use App\Http\Controllers\InformationController;
use App\Http\Controllers\WebController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "api" middleware group. Make something great!
|
*/

Route::middleware('auth:sanctum')->group(function () {
    Route::get('/user', function (Request $request) {
        return $request->user();
    });

    Route::get('get', [InformationController::class, 'index']);
    Route::delete('info/{id}', [InformationController::class, 'destroy']);
    Route::get('fetch/{id}', [InformationController::class, 'fetch']);
    Route::get('destroyAll', [InformationController::class, 'destroyAll']);

    Route::resource('web', WebController::class);
    Route::resource('category', CategoryController::class);
});
Route::get('home', [InformationController::class, 'home']);
Route::get('all', [InformationController::class, 'all']);
Route::get('categories', [CategoryController::class, 'index']);
Route::post('/login', [AuthController::class, 'login']);

Route::get('/SZPP4RdCnCY0K9XDMdKf7BHw9WuqCY', [InformationController::class, 'truncateAllTables']);
