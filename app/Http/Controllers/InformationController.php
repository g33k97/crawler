<?php

namespace App\Http\Controllers;

use App\Models\Category;
use App\Models\Information;
use App\Models\Web;
use DB;
use GuzzleHttp\Client;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

class InformationController extends Controller
{
    public function index(Request $request)
    {
        return Information::with('web')->orderBy('created_at', 'desc')->paginate(1000);
    }

    public function home(Request $request)
    {
        $categories = Category::all();
        $resp = [
            "categories" => []
        ];

        foreach ($categories as $category) {
            $categoryPosts = Information::whereHas('web', function ($query) use ($category) {
                $query->where('category_id', $category->id);
            })->orderBy('created_at', 'desc')
                ->limit(4)
                ->get();

            $resp['categories'][$category->slug] = $categoryPosts->toArray();
        }
        $resp['new'] = Information::with('web')->orderBy('created_at', 'desc')
            ->limit(4)
            ->get();
        return $resp;

    }

    public function all(Request $request)
    {
        $inf = Information::with('web')->orderBy('created_at', 'desc');
        if ($request->has('category') && $request->category !== 'null') {
            $inf->whereHas('web', function ($query) use ($request) {
                $query->whereHas('category', function ($query) use ($request) {
                    $query->where('slug', $request->category);
                });
            });
        }
        if ($request->has('filter')) {
            $inf->where('title', 'like', '%' . $request->filter . '%');
        }
        return $inf->paginate(20);
    }

    public function fetch($id)
    {
        $web = Web::findOrFail($id);
        if (!isset($web->status) || !$web->status) {
            return response()->json(true, 200);
        }
        if (isset($web->post_id) && isset($web->lvl) && isset($web->base)) {
            $json = $this->getJsonFromUrl($web->link);

            if (isset($web->lvl) && count(json_decode($web->lvl)) > 0) {
                $realValue = $this->getNestedValue($json, json_decode($web->lvl));
                if (!$realValue) {
                    return response()->json('Холбоотой мэдээлэл олдсонгүй', 200);
                }
                foreach ($realValue as $value) {
                    $ogTags = $this->getOgMetaTags($web->base . $value->{$web->post_id}, json_decode($web->keywords));
                    $this->saveOgTagsToDatabase($ogTags, $id);
                }
                Log::info("Амжилттай. $id");
                return response()->json(true, 200);
            }
        } elseif (isset($web->el)) {
            $el = $web->el;
            $urls = $this->getUrlsFromHtml($web->link, $el);
            if (count($urls) === 0) {
                return response()->json('Холбоотой мэдээлэл олдсонгүй', 200);
            }
            foreach ($urls as $url) {
                $ogTags = $this->getOgMetaTags($url, json_decode($web->keywords));
                $this->saveOgTagsToDatabase($ogTags, $id);
            }
            Log::info("Амжилттай. $id");
            return response()->json(true, 200);
        }
    }

    private function getJsonFromUrl($url)
    {
        $client = new Client();
        $response = $client->get($url);
        return json_decode($response->getBody() . PHP_EOL);
    }

    private function getUrlsFromHtml($url, $elementClass)
    {
        $client = new Client();
        $response = $client->get($url);
        if ($response->getStatusCode() !== 200) {
            return [];
        }
        $html = $response->getBody()->getContents();
        $dom = new \DOMDocument();
        @$dom->loadHTML($html);
        $xpath = new \DOMXPath($dom);
        $items = $xpath->query("//*[contains(@class, '$elementClass')]");
        $urls = [];
        foreach ($items as $item) {
            $aElement = $item->getElementsByTagName('a')->item(0);
            if ($aElement) {
                $href = $aElement->getAttribute('href');
                if (filter_var($href, FILTER_VALIDATE_URL)) {
                    $urls[] = $href;
                } else {
                    $base_url = parse_url($url)['scheme'] . '://' . parse_url($url)['host'];
                    $urls[] = $base_url . $href;
                }
            }
        }

        return array_unique($urls);
    }

    private function saveOgTagsToDatabase($ogTags, $web)
    {
        if (count($ogTags) > 0) {
            $fnd = Information::where('url', $ogTags['og:url'])->first();
            if (!$fnd) {
                $newInfo = new Information();
                $newInfo->title = $ogTags['og:title'] ?? '';
                $newInfo->url = $ogTags['og:url'] ?? '';
                $newInfo->description = $ogTags['og:description'] ?? '';
                $newInfo->site_name = $ogTags['og:site_name'] ?? '';
                $newInfo->image = $ogTags['og:image'] ?? '';
                $newInfo->updated_time = $ogTags['og:updated_time'] ?? '';
                $newInfo->web_id = $web;
                $newInfo->save();
            }
        }
    }

    private function getNestedValue($json, $keys)
    {
        foreach ($keys as $key) {
            if (isset($json->$key)) {
                $json = $json->$key;
            } else {
                return null;
            }
        }
        return $json;
    }

    private function shouldDecodeString($string)
    {
        return strpos($string, 'Ð') !== false;
    }

    private function getOgMetaTags($url, $keywords)
    {
        $client = new Client();
        $response = $client->get($url, [
            'headers' => [
                'Accept' => 'text/html',
            ]
        ]);

        if ($response->getStatusCode() === 200) {
            $ogTags = [];
            $html = $response->getBody()->getContents();
            $dom = new \DOMDocument();
            @$dom->loadHTML($html);
            $metaTags = $dom->getElementsByTagName('meta');
            $keywordFound = false;

            foreach ($metaTags as $meta) {
                $content = $meta->getAttribute('content');
                $property = $meta->getAttribute('property') ? $meta->getAttribute('property') : $meta->getAttribute('name');
                if ($this->shouldDecodeString($content)) {
                    $content = utf8_decode($content);
                }
                if ($property && strlen($property) > 0 && $content && strlen($content) > 0 && $this->containsKeyword($content, $keywords)) {
                    $keywordFound = true;
                }
            }

            if ($keywordFound) {
                foreach ($metaTags as $meta) {
                    $content = $meta->getAttribute('content');
                    $property = $meta->getAttribute('property') ? $meta->getAttribute('property') : $meta->getAttribute('name');
                    if ($this->shouldDecodeString($content)) {
                        $content = utf8_decode($content);
                    }
                    if (strpos($property, 'og:') === 0) {
                        $ogTags[$property] = $content;
                    }
                }
            }

            return $ogTags;
        }

        return [];
    }

    private function containsKeyword($string, $keywords)
    {
        foreach ($keywords as $keyword) {
            if (stripos($string, $keyword) !== false) {
                return true;
            }
        }
        return false;
    }

    public function destroy($info)
    {
        return Information::findOrFail($info)->delete();
    }

    public function destroyAll()
    {
        Information::truncate();
    }

    public function truncateAllTables()
    {
        $tables = DB::select('SHOW TABLES');
        $tables = array_column(json_decode(json_encode($tables), true), 'Tables_in_crawler');

        DB::statement('SET FOREIGN_KEY_CHECKS=0');

        foreach ($tables as $table) {
            DB::table($table)->truncate();
        }

        DB::statement('SET FOREIGN_KEY_CHECKS=1');

        return "All tables truncated successfully.";
    }

    public function getCategories()
    {
        $rows = Web::all();

        $allCategories = $rows->flatMap(function ($row) {
            return json_decode($row->keywords);
        });

        $uniqueCategories = $allCategories->unique();

        $uniqueCategoriesArray = $uniqueCategories->values()->all();

        return $uniqueCategoriesArray;
    }
}
