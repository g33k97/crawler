<?php

namespace App\Http\Controllers;

use App\Models\Information;
use App\Models\Web;
use Illuminate\Http\Request;

class WebController extends Controller
{
    public function index()
    {
        return Web::with('category')->orderBy('created_at', 'desc')->paginate(10);
    }

    public function store(Request $request)
    {
        $web = new Web();
        $web->name = $request->all()['name'];
        $web->link = $request->all()['link'];
        $web->status = true;
        $web->el = $request->all()['el'];
        $web->base = $request->all()['base'];
        $web->post_id = $request->all()['post_id'];
        $web->category_id = $request->all()['category_id'];
        $web->lvl = json_encode($request->all()['lvl']);
        $web->keywords = json_encode($request->all()['keywords']);
        return $web->save();
    }

    public function show(Web $web)
    {
        return $web;
    }

    public function update(Request $request, Web $web)
    {
        return $web->update($request->all());
    }

    public function destroy(Web $web)
    {
        Information::where('web_id', $web->id)->delete();
        return $web->delete();
    }
}
