<?php

namespace App\Http\Controllers;

use App\Models\Category;
use Illuminate\Http\Request;

class CategoryController extends Controller
{
    public function index()
    {
        return Category::orderBy('created_at', 'desc')->paginate(1000);
    }

    public function store(Request $request)
    {
        $category = new Category();
        $category->fill($request->all());
        return $category->save();
    }

    public function show(Category $category)
    {
        return $category;
    }

    public function update(Request $request, Category $category)
    {
        return $category->update($request->all());
    }

    public function destroy(Category $category)
    {
        return $category->delete();
    }
}
