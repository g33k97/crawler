<?php

namespace App\Jobs;

use App\Http\Controllers\InformationController;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class ProcessWeb implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;
    protected $webId;

    /**
     * Create a new job instance.
     */
    public function __construct($webId)
    {
        $this->webId = $webId;
    }

    public function handle()
    {
        $controller = new InformationController();
        $controller->fetch($this->webId);
    }
}
