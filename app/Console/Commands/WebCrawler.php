<?php

namespace App\Console\Commands;

use App\Jobs\ProcessWeb;
use Illuminate\Console\Command;

class WebCrawler extends Command
{
    protected $signature = 'web:crawl';
    protected $description = 'Crawl webs every 3 hours';

    public function handle()
    {
        $webIds = \DB::table('webs')->pluck('id');

        foreach ($webIds as $webId) {
            ProcessWeb::dispatch($webId);
        }
    }
}
