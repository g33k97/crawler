<?php

namespace App\Console\Commands;

use App\Http\Controllers\InformationController;
use Http;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Log;

class CrawlWebsites extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'crawl:websites';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Crawl websites and fetch data';

    /**
     * Execute the console command.
     */
    public function handle()
    {
        $websiteIds = \App\Models\Web::pluck('id');

        $token = $this->getSanctumToken();
        foreach ($websiteIds as $webId) {
            Http::withHeaders([
                'Authorization' => 'Bearer ' . $token,
            ])->get(url("/api/fetch/{$webId}"));
            $this->info("Website with web_id {$webId} crawled successfully.");
        }
        Log::info("Амжилттай");
    }

    private function getSanctumToken()
    {
        $response = Http::post(url('/api/login'), [
            'name' => 'admin',
            'password' => '123',
        ]);
        return $response->json('token');
    }
}
