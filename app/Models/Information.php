<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Information extends Model
{
    use HasFactory;

    protected $fillable = ['url', 'title', 'description', 'site_name', 'image', 'updated_time', 'web_id'];

    public function web () {
        return $this->belongsTo(Web::class);
    }
}
