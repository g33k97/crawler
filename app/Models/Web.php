<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Web extends Model
{
    use HasFactory;

    protected $fillable = ['name', 'link', 'status', 'keywords', 'el', 'post_id', 'base', 'lvl', 'category_id'];


    public function category()
    {
        return $this->belongsTo(Category::class);
    }
}
