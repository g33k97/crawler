<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration {
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::table('information', function (Blueprint $table) {
            $table->unsignedBigInteger('web_id')->index()->nullable()->default(null);

            $table->foreign('web_id')->references('id')->on('webs');
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::table('information', function (Blueprint $table) {
            $table->dropForeign(['web_id']);
        });

        Schema::table('information', function (Blueprint $table) {
            $table->dropColumn(['web_id']);
        });
    }
};
