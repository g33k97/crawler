<?php

namespace Database\Seeders;

// use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     */
    public function run(): void
    {
        \App\Models\User::factory()->create([
            'name' => 'admin',
            'email' => 'admin@example.com',
            'password' => bcrypt('123'),
        ]);




        \App\Models\Web::create([
            'name' => 'ikon',
            'link' => 'https://ikon.mn/l/2',
            'status' => true,
            'el' => 'tnldesc',
            'keywords' => json_encode(['олон'])
        ]);
        \App\Models\Web::create([
            'name' => 'khuns',
            'link' => 'https://api.khuns.mn/api/front/categories/a8bf1682-919b-47b4-964c-b091bcc22392/posts?per_page=8&page=1',
            'status' => true,
            'base' => 'https://www.khuns.mn/posts/',
            'post_id' => 'id',
            'lvl' => json_encode(['data']),
            'keywords' => json_encode(['цай'])
        ]);

        $array = [
            0 => [
                'name' => 'Мэдээлэл',
                'slug' => 'medeelel',
            ],
            1 => [
                'name' => 'Уралдаан, тэмцээн',
                'slug' => 'uraldaan',
            ],
            2 => [
                'name' => 'Дэлхийн мэдээ',
                'slug' => 'delkhii',
            ],
            3 => [
                'name' => 'Хуанли',
                'slug' => 'huanli',
            ],
            4 => [
                'name' => 'Идэвхитэй хөдөлгөөн',
                'slug' => 'idevkhitei',
            ],
            5 => [
                'name' => 'Media мэдээлэл',
                'slug' => 'media',
            ],
            6 => [
                'name' => 'Зөв зохистой хоол',
                'slug' => 'zuw',
            ],
            7 => [
                'name' => 'АМЬДРАЛЫН ХЭВ МАЯГ',
                'slug' => 'hew',
            ],
            8 => [
                'name' => 'СОНИРХОГЧДИЙН СПОРТ',
                'slug' => 'amateur',
            ],
            9 => [
                'name' => 'МЭРГЭЖЛИЙН СПОРТ',
                'slug' => 'pro',
            ],
            10 => [
                'name' => 'ТАМИРЧИД',
                'slug' => 'athlet',
            ],
            11 => [
                'name' => 'СПОРТ,АНАГААХ УХААН',
                'slug' => 'sport',
            ],
            12 => [
                'name' => 'ҮНДЭСНИЙ СПОРТ',
                'slug' => 'national',
            ],
            13 => [
                'name' => 'СПОРТЫН СТАТИСТИК',
                'slug' => 'statistic',
            ],
            14 => [
                'name' => 'НИЙТИЙН БИЕИЙН ТАМИР',
                'slug' => 'social',
            ]
        ];

        foreach ($array as $key => $value) {
            \App\Models\Category::create([
                'name' => $value['name'],
                'slug' => $value['slug'],
            ]);
        }


    }
}
